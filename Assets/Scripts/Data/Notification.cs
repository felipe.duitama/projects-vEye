﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace vEye.data
{
    public class Notification
    {
        [JsonDeserialize("user")]
        public string user;
        [JsonDeserialize("SourceName")]
        public string SourceName;
        [JsonDeserialize("password")]
        public string password;
        [JsonDeserialize("name")]
        public string name;
        [JsonDeserialize("typeUser")]
        public string typeUser;
        [JsonDeserialize("projectID")]
        public int projectID;
        [JsonDeserialize("token")]
        public string token;
    }
}
