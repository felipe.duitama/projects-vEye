﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WindowsMessage : MonoBehaviour {
    private static int myReferenceCount;

    private static WindowsMessage myInstance;

    private GameObject Boton;
    private GameObject icon;
    private GameObject textField;
    private GameObject panelButton;
    //private GameObject panelMessageImagen;
    private List<GameObject> listButton;
    private List<GameObject> listIcon;
    private string[] xboxButtons = {"360_AButton", "360_BButton", "360_XButton", "360_YButton" };

    protected string[] buttons;
    public bool used { get; set; }
    Button[] buttonsObject;

  

    public WindowsMessage()
    {
        listButton = new List<GameObject>();
        listIcon = new List<GameObject>();
    }
    
    void OnEnable()
    {
        transform.SetAsLastSibling();
    }

    void Update()
    {
        if (buttonsObject.Length > 0)
        {
            for (var i = 0; i < buttonsObject.Length; i++)
            {
                if (Input.GetButtonDown(xboxButtons[i]))
                {
                    buttonsObject[i].onClick.Invoke();
                }
            }
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
        myReferenceCount++;
        if (myReferenceCount > 1)
        {
            DestroyImmediate(this.gameObject);
            return;
        }

        myInstance = this;
        // Use this line if you need the object to persist across scenes
        //DontDestroyOnLoad(this.gameObject);
        used = false;
        icon = GameObject.Find("Icon");
        Boton = GameObject.Find("ButtonMessage");
        textField = GameObject.Find("TextMessage");
    }

    public static WindowsMessage Instance
    {
        get
        {
            return myInstance;
        }
    }

    public void showMessage(string text, params string[] buttons) {
        if (!used) {
            DestroyInstance();
            this.gameObject.SetActive(true);
            textField.GetComponentInChildren<Text>().text = text;
            createButtons(buttons);
            used = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void createButtons(string[] buttons)
    {
        GameObject Boton2;
        GameObject icon2;

        Boton.SetActive(true);
        icon.SetActive(true);

        panelButton = GameObject.Find("PanelButton");

        var i = 0;
        foreach (string button in buttons)
        {
            Boton2 = Instantiate(Boton, transform.position, transform.rotation) as GameObject;
            icon2 = Instantiate(icon, transform.position, transform.rotation) as GameObject; 
            //Guardando la instancia

            Boton2.name = button;
            Boton2.GetComponentInChildren<Text>().text = button;
            icon2.GetComponentInChildren<Image>().overrideSprite = Resources.LoadAll<Sprite>("ModalButtons")[i];

            icon2.transform.SetParent(panelButton.transform);
            Boton2.transform.SetParent(panelButton.transform);

            Boton2.transform.localScale = new Vector3(1,1,1);
            icon2.transform.localScale = new Vector3(1, 1, 1);

            listButton.Add(Boton2);
            listIcon.Add(icon2);

            Boton2 = null;
            icon2 = null;
            i++;
        }
        Boton.SetActive(false);
        icon.SetActive(false);
    }


    public Button[] getButton()
    {
        buttonsObject = panelButton.GetComponentsInChildren<Button>();
        return panelButton.GetComponentsInChildren<Button>();
     
    }

    public void str(string st) {
        Debug.Log("str: "+ st);
    }

    public void DestroyInstance()
    {
        this.gameObject.SetActive(false);
        foreach (GameObject buttonWin in listButton)
        {
            Destroy(buttonWin);
        }
        foreach (GameObject iconWin in listIcon)
        {
            Destroy(iconWin);
        }
        textField.GetComponentInChildren<Text>().text = "";
        used = false;
    }
}
