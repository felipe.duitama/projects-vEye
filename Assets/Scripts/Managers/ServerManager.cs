﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;


public class ServerManager : MonoSingleton<ServerManager>
{
    [SerializeField]
    string url = "http://192.168.100.8:3000/3dves";

    [ContextMenu("Test URL")]
    //void TestURL()
    //{
        //StartCoroutine(TestURLCrt());
    //}

    //IEnumerator TestURLCrt()
    //{
    //    var operation = API<DemoData>(testUrl);
    //    while (!operation.IsReady)
    //        yield return null;

    //    if(!operation.HasError)
    //    {
    //        Debug.Log(operation.Data.SourceName);
    //        Debug.Log(operation.JsonSource);
    //    }
    //}

    IEnumerator get<T>(string uri, OperationResult<T> operation) where T : class, new()
    {
        UnityWebRequest www = UnityWebRequest.Get(url + uri);
        yield return www.Send();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            operation.ResolveData(www.downloadHandler.text);
            operation.Status = "Ready!!!";
            operation.IsReady = true;
        }
    }

    IEnumerator post<T>(string uri, List<IMultipartFormSection> formData, OperationResult<T> operation) where T : class, new()
    {
        UnityWebRequest www = UnityWebRequest.Post(url + uri, formData);
        yield return www.Send();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            Debug.Log("Form upload complete!");
            operation.ResolveData(www.downloadHandler.text);
            operation.Status = "Ready!!!";
            operation.IsReady = true;
        }
    }

    public OperationResult<T> GET<T>(string uri) where T : class, new()
    {
        OperationResult<T> operation = new OperationResult<T>();
        StartCoroutine(get<T>(uri, operation));
        return operation;
    }

    public OperationResult<T> POST<T>(string uri, List<IMultipartFormSection> formData) where T : class, new()
    {
        OperationResult<T> operation = new OperationResult<T>();
        StartCoroutine(post<T>(uri, formData, operation));
        return operation;
    }
}
