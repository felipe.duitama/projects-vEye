﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace w2.project
{
    public class ServerInterface : MonoBehaviour
    {
        public OperationResult GetGameData()
        {
            OperationResult op = new OperationResult();
            StopAllCoroutines();
            StartCoroutine(GetServerData(op));
            return op;
        }

        IEnumerator GetServerData(OperationResult operation)
        {
            float timeToFinish = 0;

            operation.Status = "Connecting to Server... ";
            operation = ServerManager.Instance.GET<User>("/user/users");
            while (!operation.IsReady)
                yield return null;

            if (!operation.HasError)
            {
                //Debug.Log(operation.Data.SourceName);
                Debug.Log(operation.JsonSource);
            }

            while (timeToFinish < 2f)
            {
                timeToFinish += Time.deltaTime;
                operation.Progress += 0.2f * Time.deltaTime / 2f ;
                yield return null;
            }

            operation.Status = "Getting Entities... ";
            while (timeToFinish < 4f)
            {
                timeToFinish += Time.deltaTime;
                operation.Progress += 0.3f * Time.deltaTime / 2f;
                yield return null;
            }

            operation.Status = "Getting Player data... ";
            while (timeToFinish < 6f)
            {
                timeToFinish += Time.deltaTime;
                operation.Progress += Time.deltaTime / 2f;
                yield return null;
            }

            operation.Status = "Ready!!!";
            operation.Progress = 1f;

            yield return new WaitForSeconds(0.5f);

            operation.IsReady = true;
        }
    }
}
