﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace w2.project
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField]
        Image bg;

        [SerializeField]
        Image progress;

        [SerializeField]
        Text message;

        public float Progress {  set { progress.fillAmount = value; } }
        public string Message {  set { message.text = value; } }

        public OperationResult LoadLevel(string levelName)
        {
            OperationResult operation = new OperationResult();
            StopAllCoroutines();
            StartCoroutine(LoadLevelAsync(operation, levelName));
            return operation;
        }

        IEnumerator LoadLevelAsync(OperationResult operation, string sceneName)
        {
            yield return StartCoroutine(Fade(1));

            var scene = SceneManager.LoadSceneAsync(sceneName);

            while (!scene.isDone)
                yield return null;

            yield return StartCoroutine(Fade(0));

            operation.IsReady = true;
        }

        IEnumerator Fade(float targetAlpha)
        {
            while (bg.color.a != targetAlpha)
            {
                Color c = bg.color;
                c.a = Mathf.MoveTowards(c.a, targetAlpha, Time.deltaTime);
                bg.color = c;
                yield return null;
            }
        }
    }
}
