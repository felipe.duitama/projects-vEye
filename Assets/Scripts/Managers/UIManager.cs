﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    [SerializeField]
    WindowMessage windowMessage;

    [SerializeField]
    List<Window> windows;

    [SerializeField]
    Menu message;
}
