﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace w2.project
{
    public class AppManager : MonoSingleton<AppManager>
    {
        public SceneLoader Scenes;
        public ServerInterface Server;
        public App Data;

        void Start()
        {
            StartCoroutine(LoadMenu());
        }

        void initAplication() {
            //charge info app
            //init scene login
            //charge info user
        }

        IEnumerator LoadMenu()
        {
            OperationResult serverOperation = Server.GetGameData();
            
            while (!serverOperation.IsReady)
            {
                Scenes.Message = serverOperation.Status;
                Scenes.Progress = serverOperation.Progress;
                yield return null;
            }
            Scenes.Message = "";
            Scenes.Progress = 0;

            yield return null;

            OperationResult sceneOperation = Scenes.LoadLevel("Menu");

            while (!sceneOperation.IsReady)
                yield return null;
        }
    }
}